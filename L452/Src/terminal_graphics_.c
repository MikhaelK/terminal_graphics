// Header: Terminal grafics
// File Name: terminal_grafics.c
// Author: Kargapolcev M. E.
// Date: 26.06.2019
// Based on https://habr.com/ru/post/325082/ (dlinyj 28.03.2017-18:56)
// https://github.com/dlinyj/terminal_controller

#include "terminal_graphics.h"
#include "term.h"

#define home()            (printf(ESC "[H")) //Move cursor to the indicated row, column (origin at 1,1)
#define clear()           (printf(ESC "[2J")) //lear the screen, move to (1,1)
#define gotoxy(x,y)       (printf(ESC "[%d;%dH", y, x))
#define visible_cursor()  (printf(ESC "[?251"))
#define resetcolor()      (printf(ESC "[0m"))
#define set_display_atrib(color)  (printf(ESC "[%dm", color))

void frame_rendering(void);
void frame_rendering(void)
{
  home();
  set_display_atrib(B_BLUE);
  //     0123456789
  puts( "+------------------++-----------------+\n\r" //0
        "�                  ��                 �\n\r" //1
        "�-------------------------------------�\n\r" //2
        "�                  ��         �\n\r" //3
        "�                  �+---------�\n\r" //4
        "�                  ��         �\n\r" //5
        "�                  ��         �\n\r" //6
        "�                  ��         �\n\r" //7
        "�                  ��         �\n\r" //8
        "+------------------++---------+\n\r" //9
        "+--------------------+\n\r" //10
        "�                    �\n\r" //11
        "+--------------------+");   //12
  resetcolor();
}

void terminal_init(void)
{
  home();
  clear();
  gotoxy(0,0);
  frame_rendering();
}

void terminal_update(void)
{
  
  fflush(stdout);
}


