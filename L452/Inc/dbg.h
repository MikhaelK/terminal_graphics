// Header: macros for debug
// File Name: dbg.h
// Author: Kargapolcev M. E.
// Date: 13.12.2018

#ifndef DBG
#define DBG
#include "stm32l4xx_hal.h"
#include <string.h>

// debug port
#define DBG_PORT huart2
extern UART_HandleTypeDef DBG_PORT;
// debug output
//#define dbg(x)
#define dbg(x)  HAL_UART_Transmit(&DBG_PORT, (uint8_t*)(x), strlen(x), 10); \
HAL_UART_Transmit(&DBG_PORT, (uint8_t*)"\r\n", 2, 10)
#define dbgn(x)  HAL_UART_Transmit(&DBG_PORT, (uint8_t*)(x), strlen(x), 10); 

//#define debug(x,y,z) 
#endif /* DBG*/
